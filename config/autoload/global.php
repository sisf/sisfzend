<?php

return array(
    'service_manager' => array(
        'factories' => array(
            'Zend\Db\Adapter\Adapter' => 'Zend\Db\Adapter\AdapterServiceFactory',
            ),
        ),
    'acl' => array(
        'roles' => array(
            'visitante' => null,
            'usuario' => 'visitante',
            'admin' => 'usuario'
            ),
        'resources' => array(
            'Application\Controller\Index.home',
            'Admin\Controller\Transacao.mensal',
            'Admin\Controller\Index.index',
            'Admin\Controller\Usuario.registrar',
            'Admin\Controller\Usuario.perfil',
            'Admin\Controller\Usuario.atualizar',
            'Admin\Controller\Auth.index',
            'Admin\Controller\Auth.login',
            'Admin\Controller\Auth.logout',
            'Admin\Controller\Auth.recuperarsenha',
            'Admin\Controller\Categoria.index',
            'Admin\Controller\Categoria.save',
            'Admin\Controller\Categoria.delete',
            'Admin\Controller\Subcategoria.index',
            'Admin\Controller\Subcategoria.save',
            'Admin\Controller\Subcategoria.delete',
            'Admin\Controller\Transacao.index',
            'Admin\Controller\Transacao.save',
            'Admin\Controller\Transacao.delete',
            ),
'privilege' => array(
    'visitante' => array(
        'allow' => array(
                    'Application\Controller\Index.home',
            'Admin\Controller\Usuario.registrar',
            'Admin\Controller\Auth.index',
            'Admin\Controller\Auth.login',
            'Admin\Controller\Auth.recuperarsenha',
            )
),
'usuario' => array(
    'allow' => array(
        'Admin\Controller\Usuario.perfil',
        'Admin\Controller\Usuario.atualizar',
        'Admin\Controller\Auth.logout',
        'Admin\Controller\Index.index',
        'Admin\Controller\Categoria.index',
        'Admin\Controller\Categoria.save',
        'Admin\Controller\Categoria.delete', 
        'Admin\Controller\Transacao.index',
        'Admin\Controller\Transacao.save',
        'Admin\Controller\Transacao.delete',
        'Admin\Controller\Subcategoria.index',
        'Admin\Controller\Subcategoria.save',
        'Admin\Controller\Subcategoria.delete',
        'Admin\Controller\Transacao.mensal',
        )
),
'admin' => array(
    'allow' => array(
        )
    ),
)
),


    'db' => array(
        'driver' => 'Mysqli',
        'hostname' => 'mysql01.sisf.hospedagemdesites.ws',
       'username' => 'sisf',
        'password' => 'sis159159*',
        'database' => 'sisf',
        'options' => array('buffer_results' => true)
        ),

          );
