<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Session\Container;
use Zend\Mvc\Controller\AbstractActionController;
use Core\Controller\ActionController;
use Zend\View\Model\ViewModel;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelectAdapter;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Debug\Debug;
use Admin\Model\Cliente;
//use Admin\Model\Pedido;
use Application\Form\Cliente as ClienteForm;
use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\Mail\Transport\SmtpOptions;
use Admin\Model\Pagina;
use Admin\Form\Pagina as PaginaForm;
use Admin\Model\Pagfoto;
use Admin\Form\Pagfoto as PagfotoForm;
use Zend\Validator\File\Size;
use Admin\Model\Catvagas;
use Admin\Model\Vagas;
use Admin\Form\Vagas as VagasForm;
use Admin\Model\Locacao;
use Admin\Form\Locacao as LocacaoForm;
use Admin\Model\Curriculo;
use Admin\Form\Curriculo as CurriculoForm;

//use Zend\Validator\File\Size;

class IndexController extends ActionController {

    public function __construct() {

    }

    /* Funçao da página principal do site */

    public function indexAction() {
        return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/application/index/home');
    }

    public function homeAction() {

        $layout = $this->layout();
        $layout->setTemplate('layout/app');
        $view = new ViewModel();
        return $view;
    }

    
}
