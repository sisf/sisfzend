<?php

namespace Admin\Service;

use Core\Service\Service;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as AuthAdapter;
use Zend\Db\Sql\Select;
use Zend\Session\Container;

/**
 * Serviço responsável pela autenticação da aplicação
 * 
 * @category Admin
 * @package Service
 * @author  Samuel Rodrigues<samuel@agenciaw3.com.br>
 */
class Auth extends Service {

    /**
     * Adapter usado para a autenticação
     * @var Zend\Db\Adapter\Adapter
     */
    private $dbAdapter;

    /**
     * Construtor da classe
     *
     * @return void
     */
    public function __construct($dbAdapter = null) {
        $this->dbAdapter = $dbAdapter;
    }

    /**
     * Faz a autenticação dos usuários
     * 
     * @param array $params
     * @return array
     */
    public function authenticate($params) {
        if (!isset($params['nomeusuario']) || !isset($params['senha'])) {
            /*throw new \Exception("Parâmetros inválidos");*/
            $_SESSION['mensagem'] = "Login ou senha inválidos";  
        }

        $senha = md5($params['senha']);
        $auth = new AuthenticationService();
        $authAdapter = new AuthAdapter($this->dbAdapter);
        $authAdapter
                ->setTableName('usuarios')
                ->setIdentityColumn('nomeusuario')
                ->setCredentialColumn('senha')
                ->setIdentity($params['nomeusuario'])
                ->setCredential($senha);
        $result = $auth->authenticate($authAdapter);

        if (!$result->isValid()) {
            /*throw new \Exception("Login ou senha inválidos");*/
            $_SESSION['mensagem'] = "Login ou senha inválidos";        
        }

        //salva o user na sessão
        $session = $this->getServiceManager()->get('Session');
        $session->offsetSet('usuarioimoveislist', $authAdapter->getResultRowObject());

        $usuario = new Container('userDados');
        $usuario->usuario = $authAdapter->getResultRowObject();

        return true;
    }

    /**
     * Faz o logout do sistema
     *
     * @return void
     */
    public function logout() {
        $auth = new AuthenticationService();
        $session = $this->getServiceManager()->get('Session');
        $session->offsetUnset('usuarioimoveislist');
        $auth->clearIdentity();
        return true;
    }

    /**
     * Faz a autorização do usuário para acessar o recurso
     * @param string $moduleName Nome do módulo sendo acessado
     * @param string $controllerName Nome do controller
     * @param string $actionName Nome da ação
     * @return boolean
     */
    public function authorize($moduleName, $controllerName, $actionName) {
        $auth = new AuthenticationService();
        $role = 'visitante';
        if ($auth->hasIdentity()) {
            $session = $this->getServiceManager()->get('Session');
            $user = $session->offsetGet('usuarioimoveislist');
            $role = $user->role;
        }

        $resource = $controllerName . '.' . $actionName;
        $acl = $this->getServiceManager()->get('Core\Acl\Builder')->build();
        if ($acl->isAllowed($role, $resource)) {
            return true;
        }
        return false;
    }

}