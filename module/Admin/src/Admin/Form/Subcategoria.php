<?php

namespace Admin\Form;

use Zend\Form\Element;
use Zend\Form\Form;

class Subcategoria extends Form {

    public function __construct($categorias) {
        parent::__construct('subcategoria');
        $this->setAttribute('method', 'post');
        $this->setAttribute('action', str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/subcategoria/save');
        $this->setAttribute('class', "form-horizontal form-label-left");
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));
		
        /*$this->add(array(
            'name' => 'categoria_id',
            'attributes' => array(
                'type' => 'hidden',
                'value' => 50
            ),
        ));*/		

        // Preenche o array de categorias recebido pela classe para o exibir o select
         $cat;
         foreach ($categorias as $categoria) {
           $cat[$categoria['id']] = $categoria['nome'];
         }
        // Preenche o select com o array de categorias
         $select = new Element\Select('categoria_id');
        //$select->setLabel('Categoria');
         $select->setAttributes(array('class'=>'form-control'));
        $select->setValueOptions($cat);
         $this->add($select);

        $this->add(array(
            'name' => 'nome',
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control col-md-7 col-xs-12'
            ),
           
        ));


        /* $this->add(array(
          'name' => 'description',
          'attributes' => array(
          'type'  => 'textarea',
          ),
          'options' => array(
          'label' => 'Texto do categoria',
          ),
          )); */
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Enviar',
                'id' => 'submitbutton',
            ),
        ));
    }

}
