<?php
namespace Admin\Form;
 
use Zend\Form\Element; 
use Zend\Form\Form;
 
class Categoria extends Form
{
    public function __construct()
    {
        parent::__construct('categoria');
        $this->setAttribute('method', 'post');
        $this->setAttribute('action', str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/categoria/save');
        $this->setAttribute('class', "form-horizontal form-label-left");
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));
        
        $this->add(array(
            'name' => 'nome',
            'attributes' => array(
                'type'  => 'text',
                'class' => 'form-control col-md-7 col-xs-12'
            ),
            //'options' => array(
            //    'label' => 'Nome',
            //),
        ));
		
		
        // Preenche o array de categorias recebido pela classe para o exibir o select
        // $cat;
        // foreach ($marcas as $categoria) {
          // $cat[$categoria['id']] = $categoria['nome'];
        // }
        // Preenche o select com o array de categorias
        // $select = new Element\Select('marca_id');
        // $select->setLabel('Categoria Primária');
        // $select->setValueOptions($cat);
        // $this->add($select);

         $tipos['e'] = 'Entrada';
         $tipos['s'] = 'Saída';

         $tipo = new Element\Select('tipo');
         //$tipo->setLabel('Tipo');
         $tipo->setValueOptions($tipos);
         $this->add($tipo);
		
		
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Enviar',
                'id' => 'submitbutton',
            ),
        ));
    }
}