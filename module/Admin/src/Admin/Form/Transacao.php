<?php
namespace Admin\Form;
 
use Zend\Form\Element; 
use Zend\Form\Form;
 
class Transacao extends Form
{
    public function __construct($subcategorias,$t)
    {
        parent::__construct('transacao');
        $this->setAttribute('method', 'post');
        $this->setAttribute('action', str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/transacao/save');
        $this->setAttribute('class', "form-horizontal form-label-left");
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
            ),
        ));

        $this->add(array(
            'name' => 'tipo',
            'attributes' => array(
                'type'  => 'hidden',
                'value' => $t,
            ),
        ));
        
        $this->add(array(
            'name' => 'descricao',
            'attributes' => array(
                'type'  => 'text',
                'class' => 'form-control col-md-7 col-xs-12'
            ),
            //'options' => array(
            //    'label' => 'Nome',
            //),
        ));

         $this->add(array(
            'name' => 'valor',
            'attributes' => array(
                'type'  => 'text',
                'class' => 'form-control col-md-7 col-xs-12'
            ),
            //'options' => array(
            //    'label' => 'Nome',
            //),
        ));
        $this->add(array(
            'name' => 'valor',
            'attributes' => array(
                'type'  => 'text',
                'class' => 'form-control col-md-7 col-xs-12'
            ),
            //'options' => array(
            //    'label' => 'Nome',
            //),
        ));
		$cat[1] = 'Pago';
        $cat[2] = 'Não pago';
        $pago = new Element\Select('pago');
         //$oferta->setLabel('Produto em Promoção(HOME)');
        $pago->setValueOptions($cat);
        $this->add($pago);

        $cat[1] = 'Pago';
        $cat[2] = 'Não pago';
        $pago = new Element\Select('pago');
         //$oferta->setLabel('Produto em Promoção(HOME)');
        $pago->setValueOptions($cat);
        $this->add($pago);

        $subcategorias2;
        foreach ($subcategorias as $subcategoria) {
            $subcategorias2[$subcategoria['id']] = $subcategoria['cnome']." - ".$subcategoria['nome'];
        }
        // Preenche o select com o array de categorias
        $subcategoria = new Element\Select('subcategoria_id');
        //$subcategoria->setLabel('Categoria');
        $subcategoria->setValueOptions($subcategorias2);
        $this->add($subcategoria);

         $this->add(array(
            'name' => 'vencimento',
            'attributes' => array(
                'type'  => 'date',
                'class' => 'form-control col-md-7 col-xs-12',
                'value' => date('Y-m-d'),
            ),
            //'options' => array(
            //    'label' => 'Nome',
            //),
        ));

          $this->add(array(
            'name' => 'pagamento',
            'attributes' => array(
                'type'  => 'date',
                'class' => 'form-control col-md-7 col-xs-12',
                'value' => date('Y-m-d'),
            ),
            //'options' => array(
            //    'label' => 'Nome',
            //),
        ));
		
        // Preenche o array de transacaos recebido pela classe para o exibir o select
         //$cat;
        // foreach ($marcas as $transacao) {
          // $cat[$transacao['id']] = $transacao['nome'];
        // }
        // Preenche o select com o array de transacaos
        // $select = new Element\Select('marca_id');
        // $select->setLabel('Transacao Primária');
        // $select->setValueOptions($cat);
        // $this->add($select);
		
		
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type'  => 'submit',
                'value' => 'Enviar',
                'id' => 'submitbutton',
            ),
        ));
    }
}