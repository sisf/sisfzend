<?php

namespace Admin\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelectAdapter;
use Admin\Model\Subcategoria;
use Admin\Model\Categoria;
use Admin\Form\Subcategoria as SubcategoriaForm;
use Zend\Session\Container;

/**
 * Controlador que gerencia a subcategorias dos produtos
 * 
 * @category Admin
 * @package Controller
 * @author  Samuel Rodrigues <samuel@agenciaw3.com.br>
 */
class SubcategoriaController extends ActionController {

    /**
     * Mostra as subcategorias cadastrados
     * @return void
     */
    public function indexAction() {

        $session = new Container('userDados');  

        $subcategoria = $this->getTable('Admin\Model\Subcategoria');
        $sql = $subcategoria->getSql();
        $select = $sql->select()
        ->join('categorias', 'subcategorias.categoria_id = categorias.id', array('cnome'=>'nome'))
        ->where(array("subcategorias.usuario_id=".$session->usuario->id.""))
        //->group("categorias.id");
        ->order("cnome asc");

        //$categorias = $this->getTable('Admin\Model\Categoria')->fetchAll(null,null,null,null,array('nome asc'))->toArray();


        $statement = $sql->prepareStatementForSqlObject($select);
        $subcategorias = $statement->execute();


        $view = new ViewModel(array(
            'subcategorias' => $subcategorias,
            //'categorias' => $categorias
        ));
        return $view;
    }

    /**
     * Cria ou edita um post
     * @return void
     */
    public function saveAction() {
        /* $categoria = $this->getTable('Admin\Model\Categoria');
          $sql = $categoria->getSql();
          $select_categoria = $sql->select(); */

          $session = new Container('userDados');    

        $categorias = $this->getTable('Admin\Model\Categoria')->fetchAll(null,null,null,null,array('nome asc'))->toArray();

        $form = new SubcategoriaForm($categorias);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $subcategoria = new Subcategoria;
            $form->setInputFilter($subcategoria->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $form->getData();
                unset($data['submit']);
                $data['usuario_id'] = $session->usuario->id;
                //$data['post_date'] = date('Y-m-d H:i:s');
                $subcategoria->setData($data);

                $saved = $this->getTable('Admin\Model\Subcategoria')->save($subcategoria);
                return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/subcategoria');
            }
        }
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id > 0) {
            $subcategoria = $this->getTable('Admin\Model\Subcategoria')->get($id);
            $form->bind($subcategoria);
            $form->get('submit')->setAttribute('value', 'Salvar');
        }

        $view = new ViewModel(array(
            'form' => $form,

        ));
        return $view;
    }

    /**
     * Exclui um post
     * @return void
     */
    public function deleteAction() {
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id == 0) {
            throw new \Exception("Código obrigatório");
        }

        $this->getTable('Admin\Model\Subcategoria')->delete($id);
        return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/subcategoria');
    }

}