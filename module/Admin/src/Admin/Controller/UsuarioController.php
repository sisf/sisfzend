<?php

namespace Admin\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelectAdapter;
use Admin\Model\Usuario;
use Admin\Form\Usuario as UsuarioForm;
use Zend\Db\Sql\Sql;
use Zend\Session\Container;

/**
 * Controlador que gerencia as usuarios dos produtos
 *
 * @category Admin
 * @package Controller
 * @author  Samuel Rodrigues <samuel@agenciaw3.com.br>
 */
class UsuarioController extends ActionController {

    /**
     * Cria uma nova conta
     * @return void
     */
    public function registrarAction() {

        $adapter = $this->getServiceLocator()->get('DbAdapter');
        $sql = new Sql($adapter);
       

//        $translator = $this->getServiceLocator()->get('translator');
//        \Zend\Validator\AbstractValidator::setDefaultTranslator($translator);
        $form = new UsuarioForm();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $usuario = new Usuario;
            $form->setInputFilter($usuario->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $form->getData();

                $select1 = $sql->select()
                ->columns(array('nomeusuario'))
                ->from('usuarios')
                ->where(array("nomeusuario='".trim($data['nomeusuario']," ")."'"));

                $statement1 = $sql->prepareStatementForSqlObject($select1);
                $usuarioexite = $statement1->execute();

                $contador = 0;
                foreach($usuarioexite as $usus){
                    $contador++;
                }
                if($contador>0){
                     $_SESSION['mensagem'] = 'Email ja está cadastrado! <br><a href="http://imoveislis.com.br/admin/usuario/recuperarsenha">Clique para recuperar se esqueceu a sua senha.</a>';

                    return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/auth/index');
                }

                
                unset($data['btn-entrar']);
                unset($data['confirmarsenha']);
                $data['role'] = 'usuario';
                $data['valid'] = 1;
                $data['senha'] = md5($data['senha']);
                $usuario->setData($data);

                //Pega os dados postados pelo formulário HTML e os coloca em variaveis
                //if (preg_match('/tempsite.ws$|locaweb.com.br$|hospedagemdesites.ws$|websiteseguro.com$/', $_SERVER[HTTP_HOST])) {
                //substitua na linha acima a aprte locaweb.com.br por seu domínio.
                $email_from = 'sejabemvindo@sisf.com.br'; // Substitua essa linha pelo seu e-mail@seudominio
            //} else {
                //$email_from = "sejabemvindo@" . $_SERVER[HTTP_HOST];
                //    Na linha acima estamos forçando que o remetente seja 'webmaster@',
                // você pode alterar para que o remetente seja, por exemplo, 'contato@'.
            //}


            if (PATH_SEPARATOR == ';') {
                $quebra_linha = "\r\n";
            } elseif (PATH_SEPARATOR == ':') {
                $quebra_linha = "\n";
            } elseif (PATH_SEPARATOR != ';' and PATH_SEPARATOR != ':') {
                echo ('Esse script não funcionará corretamente neste servidor, a função PATH_SEPARATOR não retornou o parâmetro esperado.');
            }


            $email = $data['nomeusuario'];

            $assunto = "Cadastro no ImoveisList";


            //formato o campo da mensagem 
            $mensagem_cont = "
            <img src='http://imoveislist.com.br/assets/img/logo.png' ><br><br>
            <b>Obrigado por se cadastrar no ImoveisList!</p><br>
                <b>Agora Você pode adicionar anúncios gratuitos de imóveis para aluguel e venda.</p><br><br>

                    <b><a href='http://imoveislist.com.br/admin/auth/index'>Acesse agora!</a></p>";

                        $headers = "MIME-Version: 1.0" . $quebra_linha . "";
                        $headers .= "Content-type: text/html; charset=utf-8" . $quebra_linha . "";
                        $headers .= "From: $email_from " . $quebra_linha . "";
                        $headers .= "Return-Path: $email_from " . $quebra_linha . "";

//envia o email sem anexo 
                        //mail($email, $assunto, $mensagem_cont, $headers, "-r" . $email_from);

                        $_SESSION['mensagem'] = 'Cadastro Realizado com sucesso!';

                        $saved = $this->getTable('Admin\Model\Usuario')->save($usuario);
                        return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/auth/index');
                    }
                }
        //$id = (int) $this->params()->fromRoute('id', 0);
        //if ($id > 0) {
        //    $usuario = $this->getTable('Admin\Model\Usuario')->get($id);
        //    $form->bind($usuario);
        //    $form->get('submit')->setAttribute('value', 'Salvar');
        //}
                return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/auth/index');
                $view = new ViewModel(array(
                    'form' => $form,
                    'imobiliarias' => $imobiliarias,
                    'title'=>'Registrar'
                    ));
                return $view;
            }

            public function recuperarsenhaAction() {

        $site_session = new Container('site');

        $adapter = $this->getServiceLocator()->get('DbAdapter');

        $sql = new Sql($adapter);

        $select = $sql->select()
        ->from('estados')
        ->order(array("nome asc"));
        $statement = $sql->prepareStatementForSqlObject($select);
        $estados = $statement->execute();



        $select = $sql->select()
        ->from('imoveis_tipos')
        ->order(array("tipo asc"));
        $statement = $sql->prepareStatementForSqlObject($select);
        $tipos = $statement->execute();

        
        $email_destino = "contato@imoveislist.com.br";
        

        /* Enviar Email */
        $request = $this->getRequest();

        if ($request->isPost()) {

            //Pega os dados postados pelo formulário HTML e os coloca em variaveis
            if (preg_match('/tempsite.ws$|locaweb.com.br$|hospedagemdesites.ws$|websiteseguro.com$/', $_SERVER[HTTP_HOST])) {
                //substitua na linha acima a aprte locaweb.com.br por seu domínio.
                $email_from = 'site@' . $_SERVER[HTTP_HOST]; // Substitua essa linha pelo seu e-mail@seudominio
            } else {
                $email_from = "site@" . $_SERVER[HTTP_HOST];
                //    Na linha acima estamos forçando que o remetente seja 'webmaster@',
                // você pode alterar para que o remetente seja, por exemplo, 'contato@'.
            }


            if (PATH_SEPARATOR == ';') {
                $quebra_linha = "\r\n";
            } elseif (PATH_SEPARATOR == ':') {
                $quebra_linha = "\n";
            } elseif (PATH_SEPARATOR != ';' and PATH_SEPARATOR != ':') {
                echo ('Esse script não funcionará corretamente neste servidor, a função PATH_SEPARATOR não retornou o parâmetro esperado.');
            }

            //pego os dados enviados pelo formulário 
            $nome_para = $_POST["nome"];
            $email_de = $_POST["email"];
            $email = $email_destino;

            $assunto = "Contato do site imoveislist.com.br";

            $mensagem = $_POST["mensagem"];
            //formato o campo da mensagem 
            $mensagem_cont = "
            <b>Nome: </b><p style='text-align:justify;font-size:16px;line-height:22px'>" . $nome_para . "</p>
            <b>Email: </b><p style='text-align:justify;font-size:16px;line-height:22px'>" . $email_de . "</p>

            <b>Mensagem:</b><br><p style='text-align:justify;font-size:16px;line-height:22px'> " . $mensagem . "</p>";




            $headers = "MIME-Version: 1.0" . $quebra_linha . "";
            $headers .= "Content-type: text/html; charset=utf-8" . $quebra_linha . "";
            $headers .= "From: $email_from " . $quebra_linha . "";
            $headers .= "Return-Path: $email_from " . $quebra_linha . "";

//envia o email sem anexo 
            mail($email, $assunto, $mensagem_cont, $headers, "-r" . $email_from);

            $site_session = new Container('msg');
            $site_session->msg = 'Mensagem enviada com sucesso!';

//                echo "<script>alert('Mensagem enviada com sucesso!');</script><script>location.href='" . $_SERVER["HTTP_REFERER"] . "'</script>";
        }



//        $layout = $this->layout();
//        $layout->setTemplate('layout/app2');
        $view = new ViewModel(array(
            'estados' => $estados,
            'tipos' => $tipos,
            'title' => "Recuperar Senha",
            ));
        return $view;
    }

    /**
     * Mostra os dados do perfil
     * @return void
     */
    public function perfilAction() {
        $session = new Container('userDados');

        $usuario = $this->getTable('Admin\Model\Usuario');
        $sql = $usuario->getSql();
        $select = $sql->select()
        ->where(array("id=" . $session->usuario->id . ""));

        $statement = $sql->prepareStatementForSqlObject($select);
        $usuario = $statement->execute();

       

      

       
       


        $view = new ViewModel(array(
            'usuario' => $usuario,            
            'title' => "| Perfil"
            ));
        return $view;
    }

    /**
     * Mostra as usuarios cadastrados
     * @return void
     */
    public function indexAction() {
        $usuario = $this->getTable('Admin\Model\Usuario');
        $sql = $usuario->getSql();
        $select = $sql->select();

        $paginatorAdapter = new PaginatorDbSelectAdapter($select, $sql);
        $paginator = new Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));


        $view = new ViewModel(array(
            'usuarios' => $paginator
            ));
        return $view;
    }

    /**
     * Atualizar dados do usuario
     * @return void
     */
    public function atualizarAction() {
        $session = new Container('userDados');
        /* $translator = $this->getServiceLocator()->get('translator');
        \Zend\Validator\AbstractValidator::setDefaultTranslator($translator); */
        $form = new UsuarioForm();
        $request = $this->getRequest();
        if ($request->isPost()) {

            $usuario = new Usuario;
            $form->setInputFilter($usuario->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $form->getData();
                if ($session->usuario->senha = md5($data['senha'])) {

                    /* echo $data['confirmarsenha'];die();        */

                    $data['id'] = $session->usuario->id;
                    $data['role'] = $session->usuario->role;
                    $data['valid'] = $session->usuario->valid;
                    if (isset($data['senhanova']) && $data['senhanova'] != "") {

                        $data['senha'] = md5($data['senhanova']);
                    } else {
                        $data['senha'] = $session->usuario->senha;
                    }
                    unset($data['btnsubmit']);
                    unset($data['confirmarsenha']);
                    unset($data['senhanova']);

                    $usuario->setData($data);

                    try {
                        $saved = $this->getTable('Admin\Model\Usuario')->save($usuario);
                    } catch (\Exception $e) {

                    }

                    $_SESSION['mensagem'] = "Dados alterados com sucesso!";
                } else {
                    $_SESSION['mensagem'] = "Não foi possível alterar os dados as senha está incorreta!";
                }
            } else {
                print_r($form->getMessages());
            }
        }
        return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/usuario/perfil');
    }

    /**
     * Cria ou edita um post
     * @return void
     */
    public function saveAction() {
        $translator = $this->getServiceLocator()->get('translator');
        \Zend\Validator\AbstractValidator::setDefaultTranslator($translator);
        $form = new UsuarioForm();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $usuario = new Usuario;
            $form->setInputFilter($usuario->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $form->getData();
                unset($data['submit']);
                $data['senha'] = md5($data['senha']);
                $usuario->setData($data);

                $saved = $this->getTable('Admin\Model\Usuario')->save($usuario);
                return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/usuario');
            }
        }
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id > 0) {
            $usuario = $this->getTable('Admin\Model\Usuario')->get($id);
            $form->bind($usuario);
            $form->get('submit')->setAttribute('value', 'Salvar');
        }

        $view = new ViewModel(array(
            'form' => $form
            ));
        return $view;
    }

    /**
     * Exclui um post
     * @return void
     */
    public function deleteAction() {
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id == 0) {
            throw new \Exception("Código obrigatório");
        }

        $this->getTable('Admin\Model\Usuario')->delete($id);
        return $this->redirect()->toUrl(str_replace("/index.php", "", "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']) . '/admin/usuario');
    }

}
