<?php

namespace Admin\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelectAdapter;
use Admin\Model\Marca;
use Admin\Model\Categoria;
use Admin\Form\Categoria as CategoriaForm;
use Zend\Session\Container;

/**
 * Controlador que gerencia as categorias dos produtos
 * 
 * @category Admin
 * @package Controller
 * @author  Samuel Rodrigues <samuel@agenciaw3.com.br>
 */
class CategoriaController extends ActionController {

    /**
     * Mostra as categorias cadastrados
     * @return void
     */
    public function indexAction() {
        
        $session = new Container('userDados');

        $categoria = $this->getTable('Admin\Model\Categoria');
        $sql = $categoria->getSql();
        $select = $sql->select()
        ->where(array("categorias.usuario_id=".$session->usuario->id.""))
        ->order("nome asc");

        /*$paginatorAdapter = new PaginatorDbSelectAdapter($select, $sql);
        $paginator = new Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));*/

         $statement = $sql->prepareStatementForSqlObject($select);
        $categorias = $statement->execute();

       
        $view = new ViewModel(array(
            'categorias' => $categorias
        ));
        return $view;
    }

    /**
     * Cria ou edita um post
     * @return void
     */
    public function saveAction() {
        //$translator = $this->getServiceLocator()->get('translator');
        //\Zend\Validator\AbstractValidator::setDefaultTranslator($translator);
		
        //$marcas = $this->getTable('Admin\Model\Marca')->fetchAll()->toArray();	

        $session = new Container('userDados');	
		
        $form = new CategoriaForm();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $categoria = new Categoria;
            $form->setInputFilter($categoria->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $form->getData();
                unset($data['submit']);
                $data['usuario_id'] = $session->usuario->id;
                //$data['post_date'] = date('Y-m-d H:i:s');
                $categoria->setData($data);

                $saved = $this->getTable('Admin\Model\Categoria')->save($categoria);
                return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/categoria');
            }
        }
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id > 0) {
            $categoria = $this->getTable('Admin\Model\Categoria')->get($id);
            $form->bind($categoria);
            $form->get('submit')->setAttribute('value', 'Salvar');
        }
        
        $view = new ViewModel(array(
            'form' => $form
        ));
        return $view;
    }

    /**
     * Exclui um post
     * @return void
     */
    public function deleteAction() {
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id == 0) {
            throw new \Exception("Código obrigatório");
        }

        $this->getTable('Admin\Model\Categoria')->delete($id);
        return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/categoria');
    }

}