<?php

namespace Admin\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect as PaginatorDbSelectAdapter;
use Admin\Model\Marca;
use Admin\Model\Transacao;
use Admin\Form\Transacao as TransacaoForm;
use Zend\Session\Container;

/**
 * Controlador que gerencia as transacoes dos produtos
 * 
 * @category Admin
 * @package Controller
 * @author  Samuel Rodrigues <samuel@agenciaw3.com.br>
 */
class TransacaoController extends ActionController {

    /**
     * Mostra as transacoes cadastrados
     * @return void
     */
    public function indexAction() {

        $session = new Container('userDados');

        $transacao = $this->getTable('Admin\Model\Transacao');
        $sql = $transacao->getSql();
        $select = $sql->select()
        ->where(array("transacoes.usuario_id=".$session->usuario->id.""))
        ->join('subcategorias', 'transacoes.subcategoria_id = subcategorias.id', array('snome'=>'nome'))
        ->join('categorias', 'categorias.id = subcategorias.categoria_id', array('cnome'=>'nome'))
        ->order("pagamento desc, data desc");

        $paginatorAdapter = new PaginatorDbSelectAdapter($select, $sql);
        $paginator = new Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));

        /*$statement = $sql->prepareStatementForSqlObject($select);
        $transacoes = $statement->execute();*/


        $view = new ViewModel(array(
            'transacoes' => $paginator
            ));
        return $view;
    }

    public function mensalAction() {

        $session = new Container('userDados');

        $adapter = $this->getServiceLocator()->get('DbAdapter');

        $mes = "";
        $mes_anterior = "";

        if(isset($_GET['mes'])){
            $mes = " AND MONTH(transacoes.pagamento)=".$_GET['mes']."";
            /*montar sql para saber o saldo do mês anterior*/
            if($_GET['mes']=="1"){
                $mes_anterior = " AND MONTH(transacoes.pagamento)!=".$_GET['mes']."";
            }else{
                $mes_anterior = " AND MONTH(transacoes.pagamento)!=month(now())";
            }
        }else{
            $mes = " AND MONTH(transacoes.pagamento)=month(now())";
            /*montar sql para saber o saldo do mês anterior*/
            if(date('m')=="1"){
                $mes_anterior = " AND MONTH(transacoes.pagamento)!=".$_GET['mes']."";
            }else{
                $mes_anterior = " AND MONTH(transacoes.pagamento)!=month(now())";
            }   
        }

        $select = "SELECT categorias.nome AS cnome,subcategorias.nome AS snome,SUM(transacoes.valor) AS total
        FROM transacoes
        INNER JOIN subcategorias
        ON transacoes.subcategoria_id=subcategorias.id
        INNER JOIN categorias
        ON subcategorias.categoria_id=categorias.id
        WHERE transacoes.tipo = 'e'
        AND transacoes.pago='1'
        AND transacoes.usuario_id=".$session->usuario->id."
        AND YEAR(transacoes.pagamento)=YEAR(NOW())
        ".$mes."
        GROUP BY subcategorias.id
        ORDER BY cnome ";

        //echo $select; die();

        $entradas = $adapter->driver->getConnection()->execute($select);

        $select2 = "SELECT categorias.nome AS cnome,subcategorias.nome AS snome,SUM(transacoes.valor)*(-1) AS total
        FROM transacoes
        INNER JOIN subcategorias
        ON transacoes.subcategoria_id=subcategorias.id
        INNER JOIN categorias
        ON subcategorias.categoria_id=categorias.id
        WHERE transacoes.tipo = 's'
        AND transacoes.pago='1'
        AND transacoes.usuario_id=".$session->usuario->id."
        AND YEAR(transacoes.pagamento)=YEAR(NOW())
        ".$mes."
        GROUP BY subcategorias.id
        ORDER BY cnome ";


        $saidas = $adapter->driver->getConnection()->execute($select2);

        $select = "SELECT SUM(transacoes.valor) AS total
        FROM transacoes where
        transacoes.pago='1'
        AND transacoes.usuario_id=".$session->usuario->id."
        AND YEAR(transacoes.pagamento)=YEAR(NOW())
        ".$mes_anterior."";

        //echo $select; die();

        $saldoanterior = $adapter->driver->getConnection()->execute($select);


        $view = new ViewModel(array(
            'entradas' => $entradas,
            'saidas' => $saidas,
            'saldoanterior'=>$saldoanterior
            ));
        return $view;
    }

    /**
     * Cria ou edita um post
     * @return void
     */
    public function saveAction() {
        //$translator = $this->getServiceLocator()->get('translator');
        //\Zend\Validator\AbstractValidator::setDefaultTranslator($translator);


        //$marcas = $this->getTable('Admin\Model\Marca')->fetchAll()->toArray();

        $t = $this->params()->fromRoute('t', "");

        $session = new Container('userDados');

        $sql_entrada = "";

        if($t=='e'){
            $sql_entrada = " and categorias.tipo='e'";
        }
        if($t=='s'){
            $sql_entrada = " and categorias.tipo='s'";
        }

        $transacao = $this->getTable('Admin\Model\Subcategoria');
        $sql = $transacao->getSql();
        $select = $sql->select()
        ->where(array("subcategorias.usuario_id=".$session->usuario->id."".$sql_entrada))
        ->join('categorias', 'categorias.id = subcategorias.categoria_id', array('cnome'=>'nome'))
        ->order("cnome asc");

        /*$paginatorAdapter = new PaginatorDbSelectAdapter($select, $sql);
        $paginator = new Paginator($paginatorAdapter);
        $paginator->setCurrentPageNumber($this->params()->fromRoute('page'));*/

        $statement = $sql->prepareStatementForSqlObject($select);
        $subcategorias = $statement->execute();	

        

        $form = new TransacaoForm($subcategorias,$t);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $transacao = new Transacao;
            $form->setInputFilter($transacao->getInputFilter());
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $form->getData();

                unset($data['submit']);
                $data['usuario_id'] = $session->usuario->id;
                $data['valor'] = str_replace(".","",$data['valor']);
                $data['valor'] = str_replace(",",".",$data['valor']);

                if($data['tipo']=='s'){
                    $data['valor'] = $data['valor']*(-1);
                }
                //if($data['valor']<0){
                //   $data['tipo'] = 's';
                //}else{
                //    $data['tipo'] = 'e';
                //}
                //$data['post_date'] = date('Y-m-d H:i:s');
                $transacao->setData($data);



                $saved = $this->getTable('Admin\Model\Transacao')->save($transacao);
                return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/transacao');
            }
        }
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id > 0) {
            $transacao = $this->getTable('Admin\Model\Transacao')->get($id);

            if($transacao->tipo='s'){
               $transacao->valor = str_replace(".",",",$transacao->valor*(-1));
           }else{
               $transacao->valor = str_replace(".",",",$transacao->valor);
           }
           $form->bind($transacao);
           $form->get('submit')->setAttribute('value', 'Salvar');
       }

       $view = new ViewModel(array(
        'form' => $form,
        't' => $t
        ));
       return $view;
   }

    /**
     * Exclui um post
     * @return void
     */
    public function deleteAction() {
        $id = (int) $this->params()->fromRoute('id', 0);
        if ($id == 0) {
            throw new \Exception("Código obrigatório");
        }

        $this->getTable('Admin\Model\Transacao')->delete($id);
        return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/transacao');
    }

}