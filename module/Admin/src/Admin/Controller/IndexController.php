<?php
namespace Admin\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use Admin\Model\Transacao;
use Zend\Session\Container;

/**
 * Controlador que gerencia a index do administrador
 * 
 * @category Admin
 * @package Controller
 * @author  Samuel Rodrigues <samuel@agenciaw3.com.br>
 */
class IndexController extends ActionController
{
    /**
     * Mostra as categorias cadastradas
     * @return void
     */
    public function indexAction()
    {

        $session = new Container('userDados');

        $adapter = $this->getServiceLocator()->get('DbAdapter');

        $select = "SELECT SUM(valor) as total FROM transacoes WHERE MONTH(pagamento)=MONTH(NOW()) AND YEAR(pagamento)=YEAR(NOW()) AND transacoes.pago='1' and usuario_id=".$session->usuario->id."";

        $total = $adapter->driver->getConnection()->execute($select);

        $select2 = "SELECT SUM(valor) as entrada FROM transacoes WHERE MONTH(pagamento)=MONTH(NOW()) AND YEAR(pagamento)=YEAR(NOW()) AND transacoes.pago='1' and valor>0 and usuario_id=".$session->usuario->id."";


        $entradas = $adapter->driver->getConnection()->execute($select2);

        $select3 = "SELECT SUM(valor) as saida FROM transacoes WHERE MONTH(pagamento)=MONTH(NOW()) AND YEAR(pagamento)=YEAR(NOW()) AND transacoes.pago='1' and valor<0 and usuario_id=".$session->usuario->id."";

        $saidas = $adapter->driver->getConnection()->execute($select3);

        $select4 = "SELECT categorias.nome,(SUM(transacoes.valor)*-1) AS total
        FROM transacoes
        INNER JOIN subcategorias
        ON transacoes.subcategoria_id=subcategorias.id
        INNER JOIN categorias
        ON subcategorias.categoria_id=categorias.id
        WHERE categorias.tipo = 's' 
        AND transacoes.usuario_id=".$session->usuario->id." 
        AND YEAR(transacoes.pagamento)=YEAR(NOW())
        AND MONTH(transacoes.pagamento)=MONTH(NOW())
        AND transacoes.pago='1'
        GROUP BY categorias.id
        ORDER BY total DESC
        LIMIT 4";

        $grafico1 = $adapter->driver->getConnection()->execute($select4);
        
        $view = new ViewModel(array(
            'total' => $total,
            'entradas'=>$entradas,
            'saidas'=>$saidas,
            'grafico1'=>$grafico1
            ));
        return $view;
    }
}