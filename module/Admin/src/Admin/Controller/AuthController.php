<?php
namespace Admin\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;
use Admin\Form\Login;
use Zend\Db\Sql\Sql;
use Zend\Session\Container;
/**
* Controlador que gerencia os posts
* 
* @category Admin
* @package Controller
* @author  Samuel Rodrigues<samuel@agenciaw3.com.br>
*/
class AuthController extends ActionController
{
/**
 * Mostra o formulário de login
 * @return void
 */
public function indexAction()
{
    $form = new Login();

    /*$adapter = $this->getServiceLocator()->get('DbAdapter');
    $sql = new Sql($adapter);
    $select1 = $sql->select()
        //->columns(array('id', 'legenda', 'texto', 'imagem'))
    ->from('imobiliarias')
        //->where(array('banners.ativo=1'))
    ->order(array("nome asc"));
    $statement1 = $sql->prepareStatementForSqlObject($select1);
    $imobiliarias = $statement1->execute();*/

    $layout = $this->layout();
    $layout->setTemplate('layout/login');
    $viewModel  = new ViewModel(array(
        //'form' => $form,
        //'imobiliarias' => $imobiliarias,
        'title'=>'Anunciar Imóvel Gratis',
        'description' => "Anuncie agora seu Imóvel! Acesse agora, é totalmente gratuito!"
        ));
    return $viewModel;
}

/**
 * Envia uma nova senha para o email do usuario
 * @return void
 */
public function recuperarsenhaAction()
{
    $form = new Login();

    /*$adapter = $this->getServiceLocator()->get('DbAdapter');
    $sql = new Sql($adapter);
    $select1 = $sql->select()
        //->columns(array('id', 'legenda', 'texto', 'imagem'))
    ->from('imobiliarias')
        //->where(array('banners.ativo=1'))
    ->order(array("nome asc"));
    $statement1 = $sql->prepareStatementForSqlObject($select1);
    $imobiliarias = $statement1->execute();*/

    $request = $this->getRequest();
    if ($request->isPost()) {
         //$_SESSION['mensagem'] = "Email inválido";

       $data = $request->getPost();

       $adapter = $this->getServiceLocator()->get('DbAdapter');
       $sql = new Sql($adapter);
       $select1 = $sql->select()
        //->columns(array('id', 'legenda', 'texto', 'imagem'))
       ->from('usuarios')
       ->where(array("nomeusuario='".$data['nomeusuario']."'"));

       $statement1 = $sql->prepareStatementForSqlObject($select1);
       $usuario = $statement1->execute();

       $contador = 0;

       foreach($usuario as $us){
        $contador++;
       }

       if($contador==0){
        $_SESSION['mensagem'] = "Email inválido";
        return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/auth/recuperarsenha');
       }
       if($contador>0){
        
       }
   }



   $layout = $this->layout();
   $layout->setTemplate('layout/login');
   $viewModel  = new ViewModel(array(
        //'form' => $form,
        //'imobiliarias' => $imobiliarias,
    'title'=>'Anunciar Imóvel Gratis',
    'description' => "Anuncie agora seu Imóvel! Acesse agora, é totalmente gratuito!"
    ));
   return $viewModel;
}

/**
 * Faz o login do usuário
 * @return void
 */
public function loginAction()
{
    $request = $this->getRequest();
    if (!$request->isPost()) {
        /*throw new \Exception('Acesso inválido');*/
        $_SESSION['mensagem'] = "Email ou senha inválidos";  
        /*$_SESSION['mensagem'] = "Acesso inválido";*/
    }

    $data = $request->getPost();

    $adapter = $this->getServiceLocator()->get('DbAdapter');
    $sql = new Sql($adapter);
    
    $service = $this->getService('Admin\Service\Auth');
    /* try{*/
        $auth = $service->authenticate(array('nomeusuario' => $data['nomeusuario'], 'senha' => $data['senha']));
    /*}catch(\Exception $e){
        return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/auth/index');
    }*/
    $session = new Container('userDados');

    if(isset($session->usuario->nomeusuario)&&$session->usuario->nomeusuario==$data['nomeusuario']&&$session->usuario->senha==md5($data['senha'])){

      /*  echo "cheguei aqui e não era pra ter chegado".$session->usuario->nomeusuario;
      die(); */

      
      





      return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/index/index');


  }

  return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin/auth/index');
}

/**
 * Faz o logout do usuário
 * @return void
 */
public function logoutAction()
{
    $service = $this->getService('Admin\Service\Auth');
    $auth = $service->logout();
    unset($_SESSION['userDados']);
    return $this->redirect()->toUrl(str_replace("/index.php","","http://".$_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']).'/admin');
}
}