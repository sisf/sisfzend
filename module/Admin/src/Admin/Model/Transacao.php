<?php
namespace Admin\Model;
 
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Core\Model\Entity;
 
/**
 * Entidade Post
 * 
 * @category Admin
 * @package Model
 */
class Transacao extends Entity
{
    /**
     * Nome da tabela. Campo obrigatório
     * @var string
     */
    protected $tableName ='transacoes';
 
    /**
     * @var int
     */
    protected $id;
 
    /**
     * @var string
     */
    protected $descricao;

     /**
     * @var int
     */
    protected $valor;

     /**
     * @var string
     */
    protected $pago; 

    /**
     * @var string
     */
    protected $vencimento;   

    /**
     * @var string
     */
    protected $pagamento; 

    /**
     * @var string
     */
    protected $tipo; 

     /**
     * @var int
     */
    protected $subcategoria_id;

     /**
     * @var string
     */
    protected $data;   

    /**
     * @var int
     */
    protected $usuario_id;
 
   
    /**
     * Configura os filtros dos campos da entidade
     *
     * @return Zend\InputFilter\InputFilter
     */
    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();
 
            $inputFilter->add($factory->createInput(array(
                'name'     => 'id',
                'required' => true,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            )));
 
            $inputFilter->add($factory->createInput(array(
                'name'     => 'descricao',
                'required' => false,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 0,
                            'max'      => 150,
                        ),
                    ),
                ),
            )));

             $inputFilter->add($factory->createInput(array(
                'name'     => 'valor',
                'required' => false,
                
            )));

             $inputFilter->add($factory->createInput(array(
                'name'     => 'data',
                'required' => false,
                
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'pago',
                'required' => false,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 0,
                            'max'      => 1,
                        ),
                    ),
                ),
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'vencimento',
                'required' => false,                
            )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'pagamento',
                'required' => false,                
            )));

             $inputFilter->add($factory->createInput(array(
                'name'     => 'tipo',
                'required' => false,
                'filters'  => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name'    => 'StringLength',
                        'options' => array(
                            'encoding' => 'UTF-8',
                            'min'      => 0,
                            'max'      => 1,
                        ),
                    ),
                ),
            )));


             $inputFilter->add($factory->createInput(array(
                'name'     => 'subcategoria_id',
                'required' => false,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            )));

             $inputFilter->add($factory->createInput(array(
                'name'     => 'usuario_id',
                'required' => false,
                'filters'  => array(
                    array('name' => 'Int'),
                ),
            )));
 
 
            $this->inputFilter = $inputFilter;
        }
 
        return $this->inputFilter;
    }
}