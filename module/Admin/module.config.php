<?php

// module/Admin/con?g/module.config.php:
return array(
    'controllers' => array(//add module controllers
        'invokables' => array(
            'Admin\Controller\Index' => 'Admin\Controller\IndexController',
            'Admin\Controller\Acessos' => 'Admin\Controller\AcessosController',
            'Admin\Controller\Categoria' => 'Admin\Controller\CategoriaController',
            'Admin\Controller\Subcategoria' => 'Admin\Controller\SubcategoriaController',
            'Admin\Controller\Marca' => 'Admin\Controller\MarcaController',
            'Admin\Controller\Produto' => 'Admin\Controller\ProdutoController',
            'Admin\Controller\Profoto' => 'Admin\Controller\ProfotoController',
            'Admin\Controller\Pagina' => 'Admin\Controller\PaginaController',
            'Admin\Controller\Pagfoto' => 'Admin\Controller\PagfotoController',
            'Admin\Controller\Banner' => 'Admin\Controller\BannerController',
            'Admin\Controller\Moto' => 'Admin\Controller\MotoController',
            'Admin\Controller\Motfoto' => 'Admin\Controller\MotfotoController',
            'Admin\Controller\Configuracao' => 'Admin\Controller\ConfiguracaoController',
            'Admin\Controller\Usuario' => 'Admin\Controller\UsuarioController',
            'Admin\Controller\Auth' => 'Admin\Controller\AuthController',
            'Admin\Controller\Cliente' => 'Admin\Controller\ClienteController',
            'Admin\Controller\Pedido' => 'Admin\Controller\PedidoController',
            'Admin\Controller\Loja' => 'Admin\Controller\LojaController',
            'Admin\Controller\Lojfoto' => 'Admin\Controller\LojfotoController',
            'Admin\Controller\Faq' => 'Admin\Controller\FaqController',
            'Admin\Controller\Parcela' => 'Admin\Controller\ParcelaController',
            'Admin\Controller\Plano' => 'Admin\Controller\PlanoController',
            'Admin\Controller\Versao' => 'Admin\Controller\VersaoController',
            'Admin\Controller\Depoimento' => 'Admin\Controller\DepoimentoController',
            'Admin\Controller\Parceiro' => 'Admin\Controller\ParceiroController',
            'Admin\Controller\Newsletter' => 'Admin\Controller\NewsletterController',
            'Admin\Controller\Video' => 'Admin\Controller\VideoController',
            'Admin\Controller\Post' => 'Admin\Controller\PostController',
            'Admin\Controller\Evento' => 'Admin\Controller\EventoController',
            'Admin\Controller\Portifolio' => 'Admin\Controller\PortifolioController',
            'Admin\Controller\Locacao' => 'Admin\Controller\LocacaoController',
            'Admin\Controller\Link' => 'Admin\Controller\LinkController',
        ),
    ),
    'router' => array(
        'routes' => array(
            'admin' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/admin',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Admin\Controller',
                        'controller' => 'Auth',
                        'action' => 'index',
                        'module' => 'admin'
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                        'child_routes' => array(//permite mandar dados pela url 
                            'wildcard' => array(
                                'type' => 'Wildcard'
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(//the module can have a specific layout
     
        'template_map' => array(
           'layout/admin' => __DIR__ . '/../view/layout/admin.phtml',
           'layout/login' => __DIR__ . '/../view/layout/login.phtml',
        
        ),
        'template_path_stack' => array(
            'admin' => __DIR__ . '/../view',
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'Session' => function($sm) {
                return new Zend\Session\Container('imoveislist');
            },
            'Admin\Service\Auth' => function($sm) {
                $dbAdapter = $sm->get('DbAdapter');
                return new Admin\Service\Auth($dbAdapter);
            },
        )
    ),
        // 'db' => array( //module can have a specific db configuration
        //     'driver' => 'PDO_SQLite',
        //     'dsn' => 'sqlite:' . __DIR__ .'/../data/admin.db',
        //     'driver_options' => array(
        //         PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        //     )
        // )
);