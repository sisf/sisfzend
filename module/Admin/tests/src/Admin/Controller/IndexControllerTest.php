<?php
 
use Core\Test\ControllerTestCase;
use Admin\Controller\IndexController;
use Admin\Model\Categoria;
use Zend\Http\Request;
use Zend\Stdlib\Parameters;
use Zend\View\Renderer\PhpRenderer;
 
 
/**
 * @group Controller
 */
class IndexControllerTest extends ControllerTestCase
{
    /**
     * Namespace completa do Controller
     * @var string
     */
    protected $controllerFQDN = 'Admin\Controller\IndexController';
 
    /**
     * Nome da rota. Geralmente o nome do módulo
     * @var string
     */
    protected $controllerRoute = 'admin';
 
    /**
     * Testa o acesso a uma action que não existe
     */
    public function test404()
    {
        $this->routeMatch->setParam('action', 'action_nao_existentertrtr');
        $result = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();
        $this->assertEquals(404, $response->getStatusCode());
    }
 
    /**
     * Testa a página inicial, que deve mostrar os posts
     */
    public function testIndexAction()
    {
        // Cria posts para testar
        $categoriaA = $this->addCategoria();
        $categoriaB = $this->addCategoria();
 
        // Invoca a rota index
        $this->routeMatch->setParam('action', 'index');
        $result = $this->controller->dispatch($this->request, $this->response);
 
        // Verifica o response
        $response = $this->controller->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
 
        // Testa se um ViewModel foi retornado
        $this->assertInstanceOf('Zend\View\Model\ViewModel', $result);
 
        // Testa os dados da view
        $variables = $result->getVariables();
 
        $this->assertArrayHasKey('categorias', $variables);
 
        // Faz a comparação dos dados
        $controllerData = $variables["categorias"];
        $this->assertEquals($categoriaA->nome, $controllerData[0]['nome']);
        $this->assertEquals($categoriaB->nome, $controllerData[1]['nome']);
    }  
 
    /**
     * Adiciona uma categoria para os testes
     */
    private function addCategoria()
    {
        $categoria = new Categoria();
        $categoria->nome = 'Apple compra a Coderockr';
        
        $saved = $this->getTable('Admin\Model\Categoria')->save($categoria);
 
        return $saved;
    }
}