<?php
namespace Admin\Model;
 
use Core\Test\ModelTestCase;
use Admin\Model\Pagina;
use Zend\InputFilter\InputFilterInterface;
 
/**
 * @group Model
 */
class PaginaTest extends ModelTestCase
{
    public function testGetInputFilter()
    {
        $pagina = new Pagina();
        $if = $pagina->getInputFilter();
        //testa se existem filtros
        $this->assertInstanceOf("Zend\InputFilter\InputFilter", $if);
        return $if;
    }
 
    /**
     * @depends testGetInputFilter
     */
    public function testInputFilterValid($if)
    {
        //testa os filtros
        $this->assertEquals(3, $if->count());
 
        $this->assertTrue($if->has('id'));
        $this->assertTrue($if->has('titulo'));
        $this->assertTrue($if->has('texto'));
    }
    
    /**
     * @expectedException Core\Model\EntityException
     */
    public function testInputFilterInvalido()
    {
        //testa se os filtros estão funcionando
        $pagina = new Pagina();
        //titulo só pode ter 150 caracteres
        $pagina->titulo = 'Lorem Ipsum é simplesmente uma simulação de texto da indústria 
        tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto da indústria 
        tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressos';
        //titulo só pode ter 50 caracteres
        $pagina->texto = '';
    }        
 
    /**
     * Teste de inserção de uma categoria válida
     */
    public function testInsert()
    {
        $pagina = $this->addPagina();
 
        $saved = $this->getTable('Admin\Model\Pagina')->save($pagina);
 
        //testa o filtro de tags e espaços
        $this->assertEquals('A Apple compra a Coderockr', $saved->titulo);
        
        //testa o auto increment da chave primária
        $this->assertEquals(1, $saved->id);
    }
 
    /**
     * @expectedException Core\Model\EntityException
     */
    public function testInsertInvalido()
    {
        $pagina = new Pagina();
        $pagina->titulo = 'Lorem Ipsum é simplesmente uma simulação de texto da indústria 
        tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto da indústria 
        tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressos';
        
        $pagina->texto = '';
        
        
 
        $saved = $this->getTable('Admin\Model\Pagina')->save($pagina);
    }    
 
    public function testUpdate()
    {
        $tableGateway = $this->getTable('Admin\Model\Pagina');
        $pagina = $this->addPagina();
 
        $saved = $tableGateway->save($pagina);
        $id = $saved->id;
 
        $this->assertEquals(1, $id);
 
        $pagina = $tableGateway->get($id);
        $this->assertEquals('A Apple compra a Coderockr', $pagina->titulo);
        $this->assertEquals('texto da pagina', $pagina->texto);
 
        $pagina->titulo = 'Coderockr compra a Apple';
        $pagina->texto = 'pagina de texto';
        $updated = $tableGateway->save($pagina);
 
        $pagina = $tableGateway->get($id);
        $this->assertEquals('Coderockr compra a Apple', $pagina->titulo);
        $this->assertEquals('pagina de texto', $pagina->texto);
    }
 
    /**
     * @expectedException Core\Model\EntityException
     * @expectedExceptionMessage Could not find row 1
     */
    public function testDelete()
    {
        $tableGateway = $this->getTable('Admin\Model\Pagina');
        $pagina = $this->addPagina();
 
        $saved = $tableGateway->save($pagina);
        $id = $saved->id;
 
        $deleted = $tableGateway->delete($id);
        $this->assertEquals(1, $deleted); //numero de linhas excluidas
 
        $pagina = $tableGateway->get($id);
    }
 
    private function addPagina()
    {
        $pagina = new Pagina();
        $pagina->titulo = 'A Apple compra a Coderockr';
        $pagina->texto = 'texto da pagina';
         
        return $pagina;
    }
 
}