<?php
namespace Admin\Model;
 
use Core\Test\ModelTestCase;
use Admin\Model\Categoria;
use Zend\InputFilter\InputFilterInterface;
 
/**
 * @group Model
 */
class CategoriaTest extends ModelTestCase
{
    public function testGetInputFilter()
    {
        $categoria = new Categoria();
        $if = $categoria->getInputFilter();
        //testa se existem filtros
        $this->assertInstanceOf("Zend\InputFilter\InputFilter", $if);
        return $if;
    }
 
    /**
     * @depends testGetInputFilter
     */
    public function testInputFilterValid($if)
    {
        //testa os filtros
        $this->assertEquals(2, $if->count());
 
        $this->assertTrue($if->has('id'));
        $this->assertTrue($if->has('nome'));
    }
    
    /**
     * @expectedException Core\Model\EntityException
     */
    public function testInputFilterInvalido()
    {
        //testa se os filtros estão funcionando
        $categoria = new Categoria();
        //nome só pode ter 100 caracteres
        $categoria->nome = 'Lorem Ipsum é simplesmente uma simulação de texto da indústria 
        tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressos';
    }        
 
    /**
     * Teste de inserção de uma categoria válida
     */
    public function testInsert()
    {
        $categoria = $this->addCategoria();
 
        $saved = $this->getTable('Admin\Model\Categoria')->save($categoria);
 
        //testa o filtro de tags e espaços
        $this->assertEquals('A Apple compra a Coderockr', $saved->nome);
        
        //testa o auto increment da chave primária
        $this->assertEquals(1, $saved->id);
    }
 
    /**
     * @expectedException Core\Model\EntityException
     */
    public function testInsertInvalido()
    {
        $categoria = new Categoria();
        $categoria->nome = 'Lorem Ipsum é simplesmente uma simulação de texto da indústria 
        tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto da indústria 
        tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressos';
        
 
        $saved = $this->getTable('Admin\Model\Categoria')->save($categoria);
    }    
 
    public function testUpdate()
    {
        $tableGateway = $this->getTable('Admin\Model\Categoria');
        $categoria = $this->addCategoria();
 
        $saved = $tableGateway->save($categoria);
        $id = $saved->id;
 
        $this->assertEquals(1, $id);
 
        $categoria = $tableGateway->get($id);
        $this->assertEquals('A Apple compra a Coderockr', $categoria->nome);
 
        $categoria->nome = 'Coderockr compra a Apple';
        $updated = $tableGateway->save($categoria);
 
        $categoria = $tableGateway->get($id);
        $this->assertEquals('Coderockr compra a Apple', $categoria->nome);
    }
 
    /**
     * @expectedException Core\Model\EntityException
     * @expectedExceptionMessage Could not find row 1
     */
    public function testDelete()
    {
        $tableGateway = $this->getTable('Admin\Model\Categoria');
        $categoria = $this->addCategoria();
 
        $saved = $tableGateway->save($categoria);
        $id = $saved->id;
 
        $deleted = $tableGateway->delete($id);
        $this->assertEquals(1, $deleted); //numero de linhas excluidas
 
        $categoria = $tableGateway->get($id);
    }
 
    private function addCategoria()
    {
        $categoria = new Categoria();
        $categoria->nome = 'A Apple compra a Coderockr';
         
        return $categoria;
    }
 
}