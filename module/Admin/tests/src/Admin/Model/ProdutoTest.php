<?php
namespace Admin\Model;
 
use Core\Test\ModelTestCase;
use Admin\Model\Produto;
use Admin\Model\Marca;
use Admin\Model\Categoria;
use Zend\InputFilter\InputFilterInterface;
 
/**
 * @group Model
 */
class ProdutoTest extends ModelTestCase
{
    public function testGetInputFilter()
    {
        $produto = new Produto();
        $if = $produto->getInputFilter();
 
        $this->assertInstanceOf("Zend\InputFilter\InputFilter", $if);
        return $if;
    }
 
    /**
     * @depends testGetInputFilter
     */
    public function testInputFilterValid($if)
    {
        $this->assertEquals(14, $if->count());
 
        $this->assertTrue($if->has('id'));
        $this->assertTrue($if->has('nome'));
        $this->assertTrue($if->has('preco'));
        $this->assertTrue($if->has('preco_promocao'));
        $this->assertTrue($if->has('especificacoes'));
        $this->assertTrue($if->has('promocao'));
        $this->assertTrue($if->has('oferta'));
        $this->assertTrue($if->has('novo'));
        $this->assertTrue($if->has('descricao'));
        $this->assertTrue($if->has('subcategoria_id'));
        $this->assertTrue($if->has('marca_id'));
        $this->assertTrue($if->has('ativo'));
        $this->assertTrue($if->has('data'));
        $this->assertTrue($if->has('estoque'));
    }
    
    /**
     * @expectedException Core\Model\EntityException
     * @expectedExceptionMessage Input inválido: nome = 
     */
    public function testInputFilterInvalido()
    {
        $produto = new Produto();
        //nome não deve ter mais de 100
        $produto->nome = 'Lorem Ipsum é simplesmente uma simulação de texto da indústria 
        tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto da indústria 
        tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressos';
    }
 
    /**
     * Teste de inserção de um produto válido
     */
    public function testInsert()
    {
        $produto = $this->addProduto();
        $saved = $this->getTable('Admin\Model\Produto')->save($produto);
        $this->assertEquals('hp', $saved->nome);
        $this->assertEquals(1, $saved->id);
    }
 
    /**
     * @expectedException Zend\Db\Adapter\Exception\InvalidQueryException
     */
    public function testInsertInvalido()
    {
        $produto = new Produto();
        $produto->nome = 'teste';
        $produto->subcategoria_id = 0;
        $produto->marca_id = 0;
 
        $saved = $this->getTable('Admin\Model\Produto')->save($produto);
    }    
 
    public function testUpdate()
    {
        $tableGateway = $this->getTable('Admin\Model\Produto');
        $produto = $this->addProduto();
 
        $saved = $tableGateway->save($produto);
        $id = $saved->id;
 
        $this->assertEquals(1, $id);
 
        $produto = $tableGateway->get($id);
        $this->assertEquals('hp', $produto->nome);
 
        $produto->nome = 'impresora hp';
        $updated = $tableGateway->save($produto);
 
        $produto = $tableGateway->get($id);
        $this->assertEquals('impresora hp', $produto->nome);
 
    }
 
    /**
     * @expectedException Zend\Db\Adapter\Exception\InvalidQueryException
     * @expectedExceptionMessage Statement could not be executed
     */
    public function testUpdateInvalido()
    {
        $tableGateway = $this->getTable('Admin\Model\Produto');
        $produto = $this->addProduto();
 
        $saved = $tableGateway->save($produto);
        $id = $saved->id;
 
        $produto = $tableGateway->get($id);
        $produto->subcategoria_id = 20;
        $updated = $tableGateway->save($produto);
    }
 
    /**
     * @expectedException Core\Model\EntityException
     * @expectedExceptionMessage Could not find row 1
     */
    public function testDelete()
    {
        $tableGateway = $this->getTable('Admin\Model\Produto');
        $produto = $this->addProduto();
 
        $saved = $tableGateway->save($produto);
        $id = $saved->id;
 
        $deleted = $tableGateway->delete($id);
        $this->assertEquals(1, $deleted); //numero de linhas excluidas
 
        $produto = $tableGateway->get($id);
    }
 
    private function addCategoria()
    {
        $categoria = new Categoria();
        $categoria->nome = 'Informática';
      
        $saved = $this->getTable('Admin\Model\Categoria')->save($categoria);
 
        return $saved;
    }
 
    private function addSubcategoria() 
    {
        $categoria = $this->addCategoria();
        $subcategoria = new Subcategoria();
        $subcategoria->categoria_id = $categoria->id;
        $subcategoria->nome = 'Impressoras';
        
        $saved = $this->getTable('Admin\Model\Subcategoria')->save($subcategoria);
        
        return $saved;
    }
    
     private function addMarca()
    {
        $marca = new Marca();
        $marca->nome = 'HP';
        $marca->imagem = 'arquivo.jpg';
         
        $saved = $this->getTable('Admin\Model\Marca')->save($marca);
        
        return $saved;
    }
    
    private function addProduto() 
    {
        $subcategoria = $this->addSubcategoria();
        $marca = $this->addMarca();
        $produto = new Produto();
        $produto->subcategoria_id = $subcategoria->id;
        $produto->nome = 'hp';
        $produto->preco = '189,26';
        $produto->preco_promocao = '179,99';
        $produto->especificacoes = 'hp multifuncional';
        $produto->oferta = 0;
        $produto->novo = 1;
        $produto->promocao = 0;
        $produto->descricao = 'impressora de alta qualidade';
        $produto->marca_id = $marca->id;
        $produto->ativo = 1;
        $produto->estoque = 100;
        $produto->data = date('Y-m-d H:i:s');
 
        return $produto;
    }
}