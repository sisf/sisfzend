<?php
namespace Admin\Model;
 
use Core\Test\ModelTestCase;
use Admin\Model\Marca;
use Zend\InputFilter\InputFilterInterface;
 
/**
 * @group Model
 */
class MarcaTest extends ModelTestCase
{
    public function testGetInputFilter()
    {
        $marca = new Marca();
        $if = $marca->getInputFilter();
        //testa se existem filtros
        $this->assertInstanceOf("Zend\InputFilter\InputFilter", $if);
        return $if;
    }
 
    /**
     * @depends testGetInputFilter
     */
    public function testInputFilterValid($if)
    {
        //testa os filtros
        $this->assertEquals(3, $if->count());
 
        $this->assertTrue($if->has('id'));
        $this->assertTrue($if->has('nome'));
        $this->assertTrue($if->has('imagem'));
    }
    
    /**
     * @expectedException Core\Model\EntityException
     */
    public function testInputFilterInvalido()
    {
        //testa se os filtros estão funcionando
        $marca = new Marca();
        //nome só pode ter 100 caracteres
        $marca->nome = 'Lorem Ipsum é simplesmente uma simulação de texto da indústria 
        tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressos';
        //nome só pode ter 50 caracteres
        $marca->imagem = 'Lorem Ipsum é simplesmente uma simulação de texto da indústria 
        tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressos';
    }        
 
    /**
     * Teste de inserção de uma categoria válida
     */
    public function testInsert()
    {
        $marca = $this->addMarca();
 
        $saved = $this->getTable('Admin\Model\Marca')->save($marca);
 
        //testa o filtro de tags e espaços
        $this->assertEquals('A Apple compra a Coderockr', $saved->nome);
        
        //testa o auto increment da chave primária
        $this->assertEquals(1, $saved->id);
    }
 
    /**
     * @expectedException Core\Model\EntityException
     */
    public function testInsertInvalido()
    {
        $marca = new Marca();
        $marca->nome = 'Lorem Ipsum é simplesmente uma simulação de texto da indústria 
        tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto da indústria 
        tipográfica e de impressos. Lorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressosLorem Ipsum é simplesmente uma simulação de texto 
        da indústria tipográfica e de impressos';
        
        $marca->imagem = 'dfhdfgh';
        
        
 
        $saved = $this->getTable('Admin\Model\Marca')->save($marca);
    }    
 
    public function testUpdate()
    {
        $tableGateway = $this->getTable('Admin\Model\Marca');
        $marca = $this->addMarca();
 
        $saved = $tableGateway->save($marca);
        $id = $saved->id;
 
        $this->assertEquals(1, $id);
 
        $marca = $tableGateway->get($id);
        $this->assertEquals('A Apple compra a Coderockr', $marca->nome);
        $this->assertEquals('arquivo.jpg', $marca->imagem);
 
        $marca->nome = 'Coderockr compra a Apple';
        $marca->imagem = 'arquivo2.jpg';
        $updated = $tableGateway->save($marca);
 
        $marca = $tableGateway->get($id);
        $this->assertEquals('Coderockr compra a Apple', $marca->nome);
        $this->assertEquals('arquivo2.jpg', $marca->imagem);
    }
 
    /**
     * @expectedException Core\Model\EntityException
     * @expectedExceptionMessage Could not find row 1
     */
    public function testDelete()
    {
        $tableGateway = $this->getTable('Admin\Model\Marca');
        $marca = $this->addMarca();
 
        $saved = $tableGateway->save($marca);
        $id = $saved->id;
 
        $deleted = $tableGateway->delete($id);
        $this->assertEquals(1, $deleted); //numero de linhas excluidas
 
        $marca = $tableGateway->get($id);
    }
 
    private function addMarca()
    {
        $marca = new Marca();
        $marca->nome = 'A Apple compra a Coderockr';
        $marca->imagem = 'arquivo.jpg';
         
        return $marca;
    }
 
}